		<footer class="footer">
      <div class="footer-inner">
        <div class="footer-intro">
          <div class="footer-logo">浜田山住宅公園</div>
          <p class="footer-address">〒168-0072　東京都杉並区高井戸東3-36-35</p>
        </div>
        
        <div class="footer-cnt">
          <div class="footer-cnt--left">
            <div class="footer-cnt--time">[営業時間] <br class="pc-only2">10:00～18:00</div>
            <div class="footer-cnt--listWrap">
              <p class="footer-cnt--listLabel">[展示場までのアクセス方法]</p>
              <ul class="footer-cnt--list">
                <li><span>お車でお越しの方</span>井の頭通り、人見街道沿い。<br>環八通りから新宿方面へ約5分。</li>
                <li><span>電車でお越しの方</span>京王井の頭線「浜田山」駅より徒歩約5分。</li>
              </ul>
            </div>
          </div>
          <div class="footer-cnt--right">
            <div class="footer-cnt--right-direct">
              <a href="https://www.housing-messe.com/search_tenji01.php?utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=other" class="btn-direct" target="_blank"><span>他の住宅展示場をさがす</span></a>
            </div>
          </div>
        </div>
        
        <div class="footer-copy2Wrap">
          <ul class="footer-copy2Wrap--list">
            <li>企業概要: 株式会社サンフジ企画</li>
            <li>〒151-0053 東京都渋谷区代々木1-35-4 代々木クリスタルビル７Ｆ</li>
            <li>TEL: 03-3379-7171</li>
          </ul>
          <div class="footer-copy2">Copyright © SANFUJI All rights reserved.</div>
        </div>
      </div>
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
</body>

</html>