<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>オークラランド住宅公園</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <!-- <link rel="shortcut icon" href="assets/images/favicon.png">
  <link rel="apple-touch-icon" href="assets/images/favicon.png"> -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-N2TZ8QW');</script>
  <!-- End Google Tag Manager -->
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N2TZ8QW"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  
  
  <!-- SmartNews Ads Pixel Code -->
	<script type="text/javascript">
	!function(){if(window.SmartnewsAds=window.SmartnewsAds||{},!window.SmartnewsAds.p){var e=window.SmartnewsAds.p=function(){e.callMethod?e.callMethod.apply(e,arguments):e.queue.push(arguments)};window.SmartnewsAds._p||(window.SmartnewsAds._p=e),e.push=e,e.version="1.0.0",e.queue=[];var n=document.createElement("script");n.async=!0,n.src="//cdn.smartnews-ads.com/i/pixel.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(n,s)}}();

	SmartnewsAds.p("b8a55796c3f69ea6a96c1af4", "PageView");
	</script>
	<noscript>
	<img height="1" width="1" style="display:none;" alt="" src="https://i.smartnews-ads.com/p?id=b8a55796c3f69ea6a96c1af4&e=PageView" />
	</noscript>



  <div class="wrapper">
    <header class="header js-header pc-only2">
      <p class="header-msg">お気軽にお問い合わせください</p>
      <a href="tel:03-3706-3481" class="header-tel link">
        <img src="<?php echo $PATH;?>/assets/images/top/icon-phone.svg" alt="">
        <span>03-3706-3481</span>
        <!-- <img src="<?php echo $PATH;?>/assets/images/top/header-tel.png" alt=""> -->
      </a>
      <p class="header-date">[受付時間] 10:00 - 18:00</p>
    </header><!-- ./header -->

    <header class="header sp-only2">
      <div class="header-msgWrap">
        <p class="header-msg">
          <img src="<?php echo $PATH;?>/assets/images/top/header-msg.svg" alt="お気軽にお問い合わせください">
        </p>
        <p class="header-date">[受付]10:00-18:00</p>
      </div>
      <a href="tel:03-3706-3481" class="header-tel">
        <img src="<?php echo $PATH;?>/assets/images/top/icon-phone-sp.svg" alt="">
      </a>
      <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=8" class="btn-yellow2" target="_blank">モデルハウス見学予約</a>
    </header><!-- ./header -->

