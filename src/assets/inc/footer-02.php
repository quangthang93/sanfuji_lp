		<footer class="footer">
      <div class="footer-inner">
        <div class="footer-intro">
          <div class="footer-logo">西新井住宅公園</div>
          <p class="footer-address">〒123-0842　東京都足立区栗原1-7</p>
        </div>
        
        <div class="footer-cnt">
          <div class="footer-cnt--left">
            <div class="footer-cnt--time">[営業時間] <br class="pc-only2">10:00～18:00</div>
            <div class="footer-cnt--listWrap">
              <p class="footer-cnt--listLabel">[展示場までのアクセス方法]</p>
              <ul class="footer-cnt--list">
                <li><span>お車でお越しの方</span>環七通り　西新井陸橋脇。</li>
                <li><span>電車でお越しの方</span>東武スカイツリーライン「西新井駅」東口すぐ。</li>
                <li><span>バスでお越しの方</span>都営バス「王40」「王49」乗車、「西新井陸橋」下車すぐ。</li>
              </ul>
            </div>
          </div>
          <div class="footer-cnt--right">
            <div class="footer-cnt--right-direct">
              <a href="https://www.housing-messe.com/search_tenji01.php?utm_source=lp_nishiarai&utm_medium=lp&utm_campaign=other" class="btn-direct" target="_blank"><span>他の住宅展示場をさがす</span></a>
            </div>
          </div>
        </div>
        
        <div class="footer-copy">Copyright © SANFUJI All rights reserved.</div>
      </div>
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
</body>

</html>