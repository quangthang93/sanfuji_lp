		<footer class="footer">
      <div class="footer-inner">
        <div class="footer-intro">
          <div class="footer-logo">王子住宅公園</div>
          <p class="footer-address">〒114-0002　東京都北区王子4-28-9</p>
        </div>
        
        <div class="footer-cnt">
          <div class="footer-cnt--left">
            <div class="footer-cnt--time">[営業時間] <br class="pc-only2">10:00～18:00</div>
            <div class="footer-cnt--listWrap">
              <p class="footer-cnt--listLabel">[展示場までのアクセス方法]</p>
              <ul class="footer-cnt--list">
                <li><span>お車でお越しの方</span>北本通りから王子消防署前を右折してすぐ。（王子方面より）</li>
                <li><span>電車でお越しの方</span>東京メトロ南北線「王子神谷駅」２番出口より徒歩４分。</li>
                <li><span>バスでお越しの方</span>JR・東京メトロ王子駅より都営バス「王41、王45、王55」乗車、「王子消防署前」下車徒歩1分。</li>
              </ul>
            </div>
          </div>
          <div class="footer-cnt--right">
            <div class="footer-cnt--right-direct">
              <a href="https://www.housing-messe.com/search_tenji01.php?utm_source=lp_ouji&utm_medium=lp&utm_campaign=other" class="btn-direct" target="_blank"><span>他の住宅展示場をさがす</span></a>
            </div>
          </div>
        </div>
        
        <div class="footer-copy">Copyright © SANFUJI All rights reserved.</div>
      </div>
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
</body>

</html>