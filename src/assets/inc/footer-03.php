		<footer class="footer">
      <div class="footer-inner">
        <div class="footer-intro">
          <div class="footer-logo">石神井住宅公園</div>
          <p class="footer-address">〒177-0042　東京都練馬区下石神井1-8-4</p>
        </div>
        
        <div class="footer-cnt">
          <div class="footer-cnt--left">
            <div class="footer-cnt--time">[営業時間] <br class="pc-only2">10:00～18:00</div>
            <div class="footer-cnt--listWrap">
              <p class="footer-cnt--listLabel">[展示場までのアクセス方法]</p>
              <ul class="footer-cnt--list">
                <li><span>お車でお越しの方</span>新青梅街道から千川通りに入り、牛丼の吉野家の先左側。（田無方面から）<br>目白通りから千川通りに入り、環八通りを越えてヨークマートの先右側。（練馬方面から）</li>
                <li><span>電車でお越しの方</span>西武新宿線「井荻」「上井草」両駅から徒歩約10分。<br>西武池袋線「石神井公園」駅から徒歩約20分。</li>
                <li><span>バスでお越しの方</span>JR中央線「荻窪」駅北口より、西武バス「石神井公園駅行」（荻11）で「八成橋」下車。<br>西武池袋線「石神井公園」駅南口より、西武バス「荻窪駅行」（荻11）で「八成橋」下車。</li>
                <li><span>その他</span>千川通りと環八通り「八成橋第二」交差点そば。<br>千川通りと新青梅街道「井草4」交差点そば。</li>
              </ul>
            </div>
          </div>
          <div class="footer-cnt--right">
            <div class="footer-cnt--right-direct">
              <a href="https://www.housing-messe.com/search_tenji01.php?utm_source=lp_shakujii&utm_medium=lp&utm_campaign=other" class="btn-direct" target="_blank"><span>他の住宅展示場をさがす</span></a>
            </div>
          </div>
        </div>
        
        <div class="footer-copy">Copyright © SANFUJI All rights reserved.</div>
      </div>
    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
</body>

</html>