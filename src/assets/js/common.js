// $(function() {
jQuery(function($) {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  // Animation on scroll
  var scrollLoad = function() {
   var scroll = $(this).scrollTop();
    $('.fadeup, .fadein, .fadeleft, .fadeleft2').each(function() {
      var elemPos = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });
  }

  // Trigger header on scroll
  var triggerHeader = function() {
    if ($(this).scrollTop() > 100) {
      $('.js-header').addClass('bg-white');
    } else {
      $('.js-header').removeClass('bg-white');
    }
  }


  var matchHeight = function() {
    var $elem01 = $('.section-point--subList-ttl');
    if ($elem01.length) {
      $elem01.matchHeight();
    }
  }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    anchorLink();
    scrollLoad();
    objectFitImages();
    triggerHeader();
    matchHeight();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  $(window).scroll(function() {
    scrollLoad();
    triggerHeader();
  });

});