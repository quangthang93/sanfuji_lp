<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-05.php'; ?>
<main class="main">
  <div class="main-mv">
    <div class="main-mv--bg">
      <img class="pc-only2" src="<?php echo $PATH;?>/assets/images/top/mv.png" alt="">
      <img class="sp-only2" src="<?php echo $PATH;?>/assets/images/top/mv-sp.png" alt="">
    </div>
    <div class="main-mv--cnt">
      <div class="main-mv--cnt-msg fadeleft2">
        <div class="main-mv--cnt-msg-inner">
          <h2 class="main-mv--cnt-msg-ttl">見つかる、<br>理想の生活<br>一生の選択</h2>
          <p class="main-mv--cnt-msg-des">[ 失敗しない家づくり ]</p>
        </div>
      </div>
      <div class="main-mv--cnt-img01 fadein delay-8">
        <img src="<?php echo $PATH;?>/assets/images/top/mv-family01.svg" alt="">
      </div>
      <div class="main-mv--cnt-img02 fadein delay-12">
        <img src="<?php echo $PATH;?>/assets/images/top/mv-family02.svg" alt="">
      </div>
    </div>
  </div><!-- ./main-mv -->
  <div class="section-booking fadeup">
    <!-- <div class="ttl-booking">
      <p><span>見学予約で3,000円分のグルメギフトを、<br class="pc-only2">さらに、ご成約で10,000円分のJCBギフトカードをプレゼントします！</span></p>
    </div> -->
    <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=13&utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=reserve" class="btn-yellow" target="_blank"><span>モデルハウス見学予約はこちらから</span></a>
  </div>
  <section class="section-ideal">
    <div class="section-ideal--inner">
      <div class="section-ideal--top fadeup delay-2">
        <p class="ttl-ideal">WHAT's my ideal</p>
        <div class="section-ideal--top-cnt">
          <h3 class="section-ideal--top-ttl">理想の生活って<br class="sp-only2">なんだろう？</h3>
          <div class="section-ideal--top-box">
            <p class="section-ideal--top-box-label">家族みんなが過ごしやすい</p>
            <p class="section-ideal--top-box-des ttl-dashed">
              <span>生活に寄り添った</span>
              マイホームがある暮らし
            </p>
          </div>
        </div>
      </div><!-- ./section-ideal--top -->
      <div class="section-ideal--question fadeup">
        <div class="section-ideal--question-left">
          <img src="<?php echo $PATH;?>/assets/images/top/family03.svg" alt="">
        </div><!-- ./section-ideal--question-left -->
        <div class="section-ideal--question-right">
          <div class="section-ideal--question-right-inner">
            <p class="ttl-ideal align-right">WHAT's my life</p>
            <p class="section-ideal--question-right-subTtl">[ 今ある日常とこれからの人生を考える ]</p>
            <h3 class="section-ideal--question-right-ttl"><span>生活に寄り添った家って？</span></h3>
            <ul class="section-ideal--question-list">
              <li class="section-ideal--question-item">
                <span class="section-ideal--question-item-num">
                  <img src="<?php echo $PATH;?>/assets/images/top/question-num01.png" alt="">
                </span>
                <p class="section-ideal--question-item-desc"><span>生活スタイルに合わせた必要不可欠な機能面</span></p>
              </li>
              <li class="section-ideal--question-item">
                <span class="section-ideal--question-item-num">
                  <img src="<?php echo $PATH;?>/assets/images/top/question-num02.png" alt="">
                </span>
                <p class="section-ideal--question-item-desc"><span>家族それぞれの生活導線に合わせた間取り</span></p>
              </li>
              <li class="section-ideal--question-item">
                <span class="section-ideal--question-item-num">
                  <img src="<?php echo $PATH;?>/assets/images/top/question-num03.png" alt="">
                </span>
                <p class="section-ideal--question-item-desc"><span>5年後、10年後を見据えたメンテナンス性</span></p>
              </li>
            </ul>
          </div>
          <div class="section-booking">
            <!-- <div class="ttl-booking type2">
              <p><span>見学予約で3,000円分のグルメギフトを、<br class="pc-only2">さらに、ご成約で10,000円分のJCBギフトカードをプレゼントします！</span></p>
            </div> -->
            <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=13&utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=reserve" class="btn-yellow" target="_blank"><span>モデルハウス見学予約はこちらから</span></a>
          </div>
        </div><!-- ./section-ideal--question-right -->
      </div><!-- ./section-ideal--cnt -->
    </div><!-- ./section-ideal--inner -->
  </section><!-- ./section-ideal -->
  <section class="section-reason">
    <div class="section-reason--inner">
      <div class="section-reason--ttlWrap fadeup">
        <div class="section-reason--ttlInner">
          <p class="ttl-booking"><span>家づくりの先輩たちの声から学ぶ</span></p>
          <h3 class="section-reason--ttl">失敗しない家づくり</h3>
        </div>
      </div>
      <p class="section-reason--subTtl fadeup"><span>[こうすれば良かった！] の失敗例</span></p>
      <div class="section-reason--list fadeup">
        <div class="section-reason--item item01">
          <div class="section-reason--thumb item01">
            <div class="section-reason--msg msg01-1">収納の場所を考えてなかった！子供のおもちゃの置き場所に困る...</div>
            <div class="section-reason--msg msg01-2">朝はバタバタしているからキッチンからの導線を考えておけば良かった...</div>
            <img src="<?php echo $PATH;?>/assets/images/top/reason01.svg" alt="">
          </div>
        </div>
        <div class="section-reason--item item02">
          <div class="section-reason--thumb item02">
            <div class="section-reason--msg msg02">平日は帰宅時間や就寝時間がバラバラで生活リズムが違う...日常を考えてもっと相談すれば良かった...</div>
            <img src="<?php echo $PATH;?>/assets/images/top/reason02.svg" alt="">
          </div>
        </div>
        <div class="section-reason--item item03">
          <div class="section-reason--thumb item03">
            <div class="section-reason--msg msg03">子供が成長して、使わないスペースが沢山。よく使う部屋と部屋の距離をもっと考えておけば良かった...</div>
            <img src="<?php echo $PATH;?>/assets/images/top/reason03.svg" alt="">
          </div>
        </div>
      </div><!-- ./section-reason--reason -->
      <div class="section-reason--direct fadeup">
        <p class="ttl-dashed"><span>ライフスタイル</span>を相談して<span>現地でリアルにシミュレーション</span></p>
        <div class="section-booking">
          <!-- <div class="ttl-booking">
            <p><span>見学予約で3,000円分のグルメギフトを、<br class="pc-only2">さらに、ご成約で10,000円分のJCBギフトカードをプレゼントします！</span></p>
          </div> -->
          <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=13&utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=reserve" class="btn-yellow" target="_blank"><span>モデルハウス見学予約はこちらから</span></a>
        </div>
      </div><!-- ./section-reason--direct -->
    </div>
  </section><!-- ./section-reason -->
  <section class="section-value">
    <div class="section-value--inner">
      <div class="section-value--ttlWrap fadeup">
        <div class="section-value--ttlInner">
          <p class="section-value--subTtl"><span>GOOD VALUE</span></p>
          <h3 class="section-value--ttl">モデルハウス⾒学のメリット</h3>
        </div>
      </div>
      <p class="section-value--ttlSum fadeup">モデルハウスを見学すると、これまで気づかなかった発見があります。</p>
      <div class="section-value--cnt">
        <div class="section-value--cnt-list">
          <div class="section-value--cnt-row fadeup">
            <div class="section-value--cnt-thumb">
              <img src="<?php echo $PATH;?>/assets/images/top/value01.png" alt="">
            </div>
            <div class="section-value--cnt-infor">
              <p class="ttl-goodvalue"><span><i>実物を見る</i>ことで<i>理想の住まい</i>をリアルに掴める！</span></p>
              <ul class="section-value--listCheck">
                <li class="section-value--listCheck-item">実物のスケール感で理想の広さ・間取りが発見できる</li>
                <li class="section-value--listCheck-item">キッチンとリビングの行き来など生活導線をリアルに再現できる</li>
                <li class="section-value--listCheck-item">写真だけじゃ分からない温度感・素材感の体験で好みを発見できる</li>
                <li class="section-value--listCheck-item">理想の子供部屋・仕事部屋...家族みんなの気持ちが改めて発見できる</li>
              </ul>
            </div>
          </div>
          <div class="section-value--cnt-row fadeup">
            <div class="section-value--cnt-thumb type2">
              <img src="<?php echo $PATH;?>/assets/images/top/value02.png" alt="">
            </div>
            <div class="section-value--cnt-infor">
              <p class="ttl-goodvalue"><span><i>家づくりに必要</i>な<i>知識</i>・<i>ノウハウ</i>・<i>情報</i>が手に入る！</span></p>
              <div class="section-value--listQAWrap">
                <p class="desc">一生のうちでもっとも大きな買い物になるであろうマイホームの購入は最初は誰でも初心者です。まずは「マイホーム購入前までに必要な知識・情報・ノウハウを勉強するところから。</p>
                <p class="desc">モデルハウス見学に参加すれば、家づくりのプロである専門スタッフがどんな疑問にも丁寧にお答えしていくので、自己流で勉強しなくてもゼロから準備をはじめられます。</p>
                <p class="section-value--listQA-ttl">例えばこんな疑問にもお客様に合った最適なアドバイスをいたします！</p>
                <ul class="section-value--listQA">
                  <li class="section-value--listQA-item">実物のスケール感で理想の広さ・間取りが発見できる</li>
                  <li class="section-value--listQA-item">キッチンとリビングの行き来など生活導線をリアルに再現できる</li>
                  <li class="section-value--listQA-item">写真だけじゃ分からない温度感・素材感の体験で好みを発見できる</li>
                  <li class="section-value--listQA-item">理想の子供部屋・仕事部屋...家族みんなの気持ちが改めて発見できる</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="section-reason--direct fadeup">
          <p class="ttl-dashed"><span>ライフスタイル</span>を相談して<span>現地でリアルにシミュレーション</span></p>
          <br>
          <div class="section-booking">
            <!-- <div class="ttl-booking">
              <p><span>見学予約で3,000円分のグルメギフトを、<br class="pc-only2">さらに、ご成約で10,000円分のJCBギフトカードをプレゼントします！</span></p>
            </div> -->
            <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=13&utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=reserve" class="btn-yellow" target="_blank"><span>モデルハウス見学予約はこちらから</span></a>
          </div>
        </div><!-- ./section-reason--direct -->
      </div><!-- ./section-value--cnt -->
    </div>
  </section><!-- ./section-value -->
  <section class="section-point">
    <h3 class="section-point--ttl fadeup">住宅展⽰場のここがすごい！</h3>
    <div class="section-point--inner">
      <div class="section-point--list">
      	<div class="section-point--item point01 fadeup">
      	  <div class="section-point--item-ttlWrap">
      	    <div class="section-point--item-num">
      	      <img src="<?php echo $PATH;?>/assets/images/top/num-point01.png" alt="">
      	    </div>
      	    <div class="section-point--item-ttl">
      	      <p class="section-point--item-ttl-big">一流大手ハウスメーカーのモデルハウスをたくさん見られる</p>
      	      <p class="section-point--item-ttl-small">最新住宅設備が備わった複数のモデルハウスを見学・比較できます。</p>
      	    </div>
      	    <!-- point02-1.png -->
      	  </div>
      	  <div class="section-point--item-thumb">
      	    <img class="pc-only2" src="<?php echo $PATH;?>/assets/images/top/point01.png" alt="">
      	    <img class="sp-only2" src="<?php echo $PATH;?>/assets/images/top/point01-sp.png" alt="">
      	  </div>
      	</div>
      	<div class="section-point--item point02 fadeup">
      		<div class="section-point--item-ttlWrap">
      	    <div class="section-point--item-num">
      	      <img src="<?php echo $PATH;?>/assets/images/top/num-point02.png" alt="">
      	    </div>
      	    <div class="section-point--item-ttl">
      	      <p class="section-point--item-ttl-big">モデルハウス見学の合間にイベントに参加できる</p>
      	      <p class="section-point--item-ttl-small">住宅展示場では家族で楽しめるイベントが開催！子供にとっても楽しいおでかけになります。</p>
      	    </div>
      	  </div>
      	  <div class="section-point--subListWrap">
      	  	<div class="section-point--subList">
      	  		<div class="section-point--subList-child">
      	  			<div class="section-point--subList-thumbWrap">
      	  				<div class="section-point--subList-thumb">
      	  					<img src="<?php echo $PATH;?>/assets/images/top/point02-1.png" alt="">
      	  				</div>
      	  				<h4 class="section-point--subList-ttl"><span>親子で体を動かす<br>スポーツ系イベント</span></h4>
      	  			</div>
      	  		</div>
      	  		<div class="section-point--subList-child">
      	  			<div class="section-point--subList-thumbWrap">
      	  				<div class="section-point--subList-thumb">
      	  					<img src="<?php echo $PATH;?>/assets/images/top/point02-2.png" alt="">
      	  				</div>
      	  				<h4 class="section-point--subList-ttl"><span>自分で手を動かす<br>ワークショップ・工作系イベント</span></h4>
      	  			</div>
      	  		</div>
      	  		<div class="section-point--subList-child">
      	  			<div class="section-point--subList-thumbWrap">
      	  				<div class="section-point--subList-thumb">
      	  					<img src="<?php echo $PATH;?>/assets/images/top/point02-3.png" alt="">
      	  				</div>
      	  				<h4 class="section-point--subList-ttl"><span>こんなこと出来るの⁉<br>体験系イベント</span></h4>
      	  			</div>
      	  		</div>
      	  	</div><!-- ./section-point--subList -->
      	  	<div class="section-point--subInfor">
      	  		<p class="desc">※モデルハウス・イベント会場内におきましては、アルコール消毒や従業員のマスク着用等、感染症予防対策を徹底し営業しております。</p>
      	  		<p class="desc">※イベント内容および開催日程は、<a class="link-inline" href="https://www.housing-messe.com/hamadayama.html?utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=event#event-link" target="_blank">住宅展示場サイト</a>をご確認ください。</p>
      	  	</div>
      	  </div><!-- ./section-point--subListWrap -->
      	</div>
      </div>
    </div>
    <div class="section-point--booking fadeup">
      <div class="section-booking">
        <!-- <div class="ttl-booking">
          <p><span>見学予約で3,000円分のグルメギフトを、<br class="pc-only2">さらに、ご成約で10,000円分のJCBギフトカードをプレゼントします！</span></p>
        </div> -->
        <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=13&utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=reserve" class="btn-yellow" target="_blank"><span>モデルハウス見学予約はこちらから</span></a>
      </div>
    </div><!-- ./section-reason--direct -->
  </section><!-- ./section-point -->


  <section class="section-access">
  	<h3 class="section-access--ttl fadeup">
  		<span>城西エリアのおすすめ展示場</span>
  	</h3>
  	<div class="section-access--infor fadeup">
  		<div class="section-access--infor-thumb">
  			<img class="cover" src="<?php echo $PATH;?>/assets/images/top/access05.jpg" alt="">
  		</div>
  		<div class="section-access--infor-cnt">
  			<h4 class="section-access--infor-name">浜田山住宅公園</h4>
  			<p class="section-access--infor-summary">開設から20年以上が経過した総合住宅展示場です。都会の家づくりを知り尽くした住宅メーカーの魅力ある提案がいっぱいのモデルハウスは必見です。</p>
  			<div class="section-access--infor-table">
  				<table>
  					<tr>
  						<th>所在地:</th>
  						<td>〒168-0072　東京都杉並区高井戸東3-36-35</td>
  					</tr>
  					<tr>
  						<th>営業時間:</th>
  						<td>10:00～18:00</td>
  					</tr>
  				</table>
  			</div>
  			<div class="section-access--infor-link">
  				<a class="link-access" href="https://www.housing-messe.com/hamadayama.html?utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=exhibition" target="_blank">公式サイトを見る</a>
  			</div>
  		</div>
  	</div>
  	<div class="section-access--map fadeup">
  		<div class="section-access--map-iframe">
  			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3240.642552317619!2d139.62164795098712!3d35.685802480096044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f2166698c401%3A0x8663aec5bc173989!2z44CSMTY4LTAwNzIg5p2x5Lqs6YO95p2J5Lim5Yy66auY5LqV5oi45p2x77yT5LiB55uu77yT77yW4oiS77yT77yV!5e0!3m2!1sja!2sjp!4v1623814590921!5m2!1sja!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
  		</div>
  		<div class="section-access--map-direct">
  			<a href="https://goo.gl/maps/LkWn6BPvUu6eDcbC7" class="link-blank" target="_blank">大きい地図で見る</a>
  		</div>
  	</div>
  </section><!-- ./section-access -->

  <div class="section-entry">
    <div class="section-booking fadeup">
      <!-- <div class="ttl-booking">
        <p><span>見学予約で3,000円分のグルメギフトを、<br class="pc-only2">さらに、ご成約で10,000円分のJCBギフトカードをプレゼントします！</span></p>
      </div> -->
      <a href="https://www.housing-messe.com/reserve/tokyo/?mode=reserve&id_exhibition=13&utm_source=lp_hamadayama&utm_medium=lp&utm_campaign=reserve" class="btn-yellow" target="_blank"><span>モデルハウス見学予約はこちらから</span></a>
    </div>
  </div><!-- ./section-entry -->

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-05.php'; ?>